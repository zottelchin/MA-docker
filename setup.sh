#!/bin/bash

echo "Starting Setup Process"

# Fail if no repo url was supplied
if [ $# -ne 1 ]; then
	echo "You need to supply a repo to clone"
	exit 1
fi

# Removing old dir
echo "Cleanup working directory..."
rm -rf work
mkdir work

# Clone Repo
echo "Cloning $1 "
git clone "$1" work/repo

touch commits.txt

# Finished
echo "Setup Complete!"
echo "To run the analysis see RUN.md"