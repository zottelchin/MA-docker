# Instructions for running Analysis on a git repo

To create a list with all commits that are reachable from master branch (branchname can be replaced to fit your usecase), run:
```bash
git rev-list master > ../../commits.txt 
```